terraform {
  required_providers {
    heroku = {
      source = "heroku/heroku"
      version = "4.5.1"
    }
  }
}

provider "heroku" {
  email   = "ioricloud@gmail.com"
  api_key = var.key
}

resource "heroku_app" "gobot" {
  name   = var.app_name
  region = "us"
  #stack  = "container"

  sensitive_config_vars = {
    "TOKEN_TELEGRAM" = "2025258055:AAH5e1mg_85lZrCS2Y0-t-5bWx6Z-TJhaKg"
  }
}

resource "heroku_build" "gobotapp" {
  app = heroku_app.gobot.name

  source {
    path = "./application"
  }
}

resource "heroku_formation" "gobotup" {
  app      = heroku_app.gobot.name
  type     = "web"
  quantity = 1
  size     = "Standard-1x"
  depends_on = [
    heroku_build.gobotapp
  ]
}

output "app_url" {
  value = "https://${heroku_app.gobot.name}.herokuapp.com"
}