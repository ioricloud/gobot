package main

import (
	"fmt"
	"log"
	"os"

	"github.com/yanzay/tbot/v2"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load(".env")
	bot := tbot.New(os.Getenv("TOKEN_TELEGRAM"),
		tbot.WithWebhook("https://gobotapp.herokuapp.com/", ":8080"))

	c := bot.Client()
	bot.HandleMessage("/info", func(m *tbot.Message) {
		c.SendChatAction(m.Chat.ID, tbot.ActionTyping)
		c.SendMessage(m.Chat.ID, info())
	})

	bot.HandleMessage("/chat", func(m *tbot.Message) {
		c.SendChatAction(m.Chat.ID, tbot.ActionTyping)
		c.SendMessage(m.Chat.ID, chat())
	})

	bot.HandleMessage("/drive", func(m *tbot.Message) {
		c.SendChatAction(m.Chat.ID, tbot.ActionTyping)
		c.SendMessage(m.Chat.ID, drive())
	})

	bot.HandleMessage("/cofre", func(m *tbot.Message) {
		c.SendChatAction(m.Chat.ID, tbot.ActionTyping)
		c.SendMessage(m.Chat.ID, cofre())
	})

	bot.HandleMessage("/wiki", func(m *tbot.Message) {
		c.SendChatAction(m.Chat.ID, tbot.ActionTyping)
		c.SendMessage(m.Chat.ID, wiki())
	})

	bot.HandleMessage("/notifier", func(m *tbot.Message) {
		c.SendChatAction(m.Chat.ID, tbot.ActionTyping)
		c.SendMessage(m.Chat.ID, notifier())
	})

	fmt.Println("Bot in GO")
	err = bot.Start()
	if err != nil {
		log.Fatal(err)
	}
}

func info() string {
	log.Println("Funcao info")
	txt := `Bot Info Channel
	Comando geral para todas infos.
	Todos os comandos.
	/chat - Link para o Chat Privado.
	/drive - Link para o Google Drive da Infra.
	/cofre - Link para o Cofre de Senhas.
	/wiki - Link para wiki da Infra.
	/notifier - Link para Notificacoes Jenkins.
	`
	return txt
}

func chat() string {
	log.Println("Funcao chat")
	txt := "Link para o chat privado da Infra\n"
	lnk := "https://meet.google.com/usz-ytpu-boj"

	full := txt + lnk
	return full
}

func drive() string {
	log.Println("Funcao drive")
	txt := `Link para o google drive da Infra.
	Link abaixo.
	`
	lnk := "https://drive.google.com/drive/folders/1v6YNNL5Q1kZDkR5gDGP62f7NcwBA80sc"
	full := txt + lnk
	return full
}

func cofre() string {
	log.Println("Funcao cofre")
	txt := `Link para sistema de Cofre.
	Link totalmente privado e necessita de acesso via vpn.
	Link abaixo:
	`
	lnk := "https://cofre.intranet.leadfortaleza.com.br/"
	full := txt + lnk
	return full
}

func wiki() string {
	log.Println("Funcao wiki")
	txt := `Link para Wiki do Lead - Setor Infra.
	Link totalmente privado e necessita de acesso permitido.
	Link abaixo:
	`
	lnk := "https://wiki.leadfortaleza.com.br/pt-br/infra-cloud"
	full := txt + lnk
	return full
}

func notifier() string {
	log.Println("Funcao notifier")
	txt := `Link de arquitetura de software sob os olhares da Amazon.
	Link a parte de nos e dedicamos este comando para saber mais sobre.
	Link abaixo:
	`
	lnk := "https://aws.amazon.com/architecture/well-architected/?wa-lens-whitepapers.sort-by=item.additionalFields.sortDate&wa-lens-whitepapers.sort-order=desc"
	full := txt + lnk
	return full
}
