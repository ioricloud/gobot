module go-telebot

go 1.15

require (
	github.com/joho/godotenv v1.4.0
	github.com/yanzay/tbot/v2 v2.2.0
)
